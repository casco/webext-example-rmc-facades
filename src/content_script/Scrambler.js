class Scrambler {
  constructor() {
    this.hToScramble = null;
  }

  addClickListeners() {
    const me = this;
    document.querySelectorAll("h1,h2,h3,h4").forEach(function(elem) {
      elem.addEventListener("mousedown", function(e) {
        me.clicked(e);
      });
    });
  }

  clicked(event) {
    this.hToScramble = event.currentTarget;
    RMCProxyFactory.backgroundProxy().userWantsToScramble({text : this.hToScramble.innerHTML})
  }

  /**
   * Replace the content of the heading previously clicked by the user
   * @param {String} txt - the text to use as a replacement 
   */
  scrambleWith(txt) {
    if (this.hToScramble != null) {
      this.hToScramble.innerHTML = txt;
      this.hToScramble = null;
    }
  }
}
