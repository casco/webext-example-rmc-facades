/** 
* This class implement the Facade pattern. It is the only entry point
* in the content script subsystem for remote messages from the background scripts. 
* See content.js to learn how I receive messages from a remote object (the background scripts)
* All my methods have one argument (arguments)
*/

let contentFacadeSingleton = null;

class ContentFacade extends Facade {

    constructor() {
        super();
        this.scrambler = new Scrambler();
    }

    static getSingleton() {
        if (contentFacadeSingleton == null) {
            contentFacadeSingleton = new ContentFacade();
        }  
        return contentFacadeSingleton
    }

    attach() {
        this.scrambler.addClickListeners();
    }

   /**
   * Replace the content of the heading previously clicked by the user
   * This message was sent from the background script
   * @param {Object {alternativeText: String}} args - the text to use as a replacement is a property of this args object
   */
    scrambleWith(args) {
        this.scrambler.scrambleWith(args.alternativeText);
    }
   
}